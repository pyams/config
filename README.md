
# PyAMS configuration file

This project only contains a single **buildout.cfg** file.

This is a zc.buildout configuration file which is updated every time a new package is released.
Minor and major releases are updated according to internal modules releases...

This file can then be referenced into your applications, environments and own projects,
using a specific tag or the *master* branch if you want to use the development modules, as
shown below:

```
[buildout]
extends = https://gitlab.com/pyams/config/-/raw/2.28.0/pyams-v2.cfg
```

or:
```
[buildout]
extends = https://gitlab.com/pyams/config/-/raw/master/pyams-v2.cfg
```

Please be warned however that development branch should only be used for your own PyAMS
development, and may contain references to yet unpublished releases!
